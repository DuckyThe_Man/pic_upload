# 图床

#### 介绍
基于SpringBoot的图片上传管理网站<br>
前端 Thymeleaf<br>
图片上传组件 LayUI<br>

相同MD5图片只保存一份<br>
图片上传者可修改图片的访问权限<br><br><br>
### [用户登录]
![用户登录](https://images.gitee.com/uploads/images/2020/0409/184500_b84e80bb_2265904.jpeg "Snipaste_2020-04-09_18-37-00.jpg")<br>
### [图片上传]
![图片上传](https://images.gitee.com/uploads/images/2020/0409/184509_4800cf35_2265904.jpeg "Snipaste_2020-04-09_18-37-27.jpg")<br>
### [图片管理]
![图片管理](https://images.gitee.com/uploads/images/2020/0409/184903_3992c1aa_2265904.jpeg "Snipaste_2020-04-09_18-38-09.jpg")<br>
### [查看地址]
![查看地址](https://images.gitee.com/uploads/images/2020/0409/185125_d6dabc51_2265904.jpeg "Snipaste_2020-04-09_18-38-22.jpg")<br>
### [禁止查看]
![禁止查看](https://images.gitee.com/uploads/images/2020/0409/185150_510a67c6_2265904.jpeg "Snipaste_2020-04-09_18-39-11.jpg")<br>

