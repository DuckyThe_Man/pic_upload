package com.hongpeiming.picupload.service;

import com.hongpeiming.picupload.pojo.Pic;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PicService {
    //获取所有
    List<Pic> showAllPic();

    //获取我的
    List<Pic> showMyPic(Integer userId);

    //通过主键查询
    Pic findPicByKey(Integer picId);

    //上传单个图片
    Integer uploadSinglePic(Pic pic);

    //查询是否有重复MD5
    boolean isRepeatMD5(String MD5);

    //查询是否需要删除文件
    boolean isNeedDelete(String MD5);

    //查询相同MD5文件的路径
    String findTheSameMD5FilePath(String MD5);

    //删除图片
    Boolean deletePic(Integer picId);

    //修改备注
    void editremark(Integer picId,String remark);
    //修改权限
    void changestatus(Integer picId,Integer value);

}
