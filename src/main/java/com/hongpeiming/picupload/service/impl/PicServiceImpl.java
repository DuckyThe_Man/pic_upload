package com.hongpeiming.picupload.service.impl;

import com.hongpeiming.picupload.mapper.PicMapper;
import com.hongpeiming.picupload.pojo.Pic;
import com.hongpeiming.picupload.pojo.PicExample;
import com.hongpeiming.picupload.service.PicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PicServiceImpl implements PicService {
    @Autowired
    PicMapper picMapper;

    @Override
    public List<Pic> showAllPic() {
        return null;
    }

    @Override
    public List<Pic> showMyPic(Integer userId) {
        PicExample example = new PicExample();
        PicExample.Criteria criteria = example.createCriteria();
        criteria.andPicUploaderIdEqualTo(userId);
        List<Pic> picList = picMapper.selectByExample(example);
        return picList;
    }

    @Override
    public Pic findPicByKey(Integer picId) {
        Pic pic = null;
        try {
            pic = picMapper.selectByPrimaryKey(picId);
            System.out.println(pic.getPicStatus());
            return pic;
        }catch (Exception e)
        {
            System.out.println(e);
            pic = new Pic();
            pic.setPicStatus(10);
            return pic;
        }
    }

    @Override
    public Integer uploadSinglePic(Pic pic) {
        return picMapper.insert(pic);
    }

    @Override
    public boolean isRepeatMD5(String MD5) {
        PicExample example = new PicExample();
        PicExample.Criteria criteria = example.createCriteria();
        criteria.andPicMd5EqualTo(MD5);
        long theSameNum  = picMapper.countByExample(example);
        if (theSameNum > 0)
        {
            return true;
        }
        return false;
    }

    @Override
    public boolean isNeedDelete(String MD5) {
        PicExample example = new PicExample();
        PicExample.Criteria criteria = example.createCriteria();
        criteria.andPicMd5EqualTo(MD5);
        long theSameNum  = picMapper.countByExample(example);
        if (theSameNum == 1)
        {
            return true;
        }
        return false;
    }

    @Override
    public String findTheSameMD5FilePath(String MD5) {
        PicExample example = new PicExample();
        PicExample.Criteria criteria = example.createCriteria();
        criteria.andPicMd5EqualTo(MD5);
        try {
            Pic pic = picMapper.selectByExample(example).get(0);
            return pic.getPicUrlPath();
        }catch (Exception e)
        {
            System.out.println("找不到源路径");
            return null;
        }
    }

    @Override
    public Boolean deletePic(Integer picId) {
        picMapper.deleteByPrimaryKey(picId);
        return true;
    }

    @Override
    public void editremark(Integer picId, String remark) {
        Pic pic = new Pic();
        pic.setPicId(picId);
        pic.setPicRemark(remark);
        picMapper.updateByPrimaryKeySelective(pic);
    }

    @Override
    public void changestatus(Integer picId, Integer value) {
        Pic pic = new Pic();
        pic.setPicId(picId);
        pic.setPicStatus(value);
        picMapper.updateByPrimaryKeySelective(pic);
    }
}
