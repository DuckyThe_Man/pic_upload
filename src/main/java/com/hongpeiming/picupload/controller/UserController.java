package com.hongpeiming.picupload.controller;

import com.hongpeiming.picupload.pojo.User;
import com.hongpeiming.picupload.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class UserController {
    @Autowired
    UserService userService;
    @RequestMapping("/")
    public String main()
    {
        return "redirect:/UserManager";
    }


    //模拟登录账号
    @RequestMapping("/Login/{UserId}")
    public String main(@PathVariable("UserId")Integer userId, Model model, HttpSession session)
    {
        //登录用户
//        Integer userId = 1;

        try {
            User user = userService.selectUserById(userId);
            if (user!=null)
            {
                session.setAttribute("User",user);
                session.setAttribute("userId",user.getUserId());
            }
        }catch (Exception e)
        {

        }


        return "redirect:/ManagerPic";
    }

    //用户信息页面
    @RequestMapping("/UserManager")
    public String UserManager(Model model, HttpSession session)
    {
        System.out.print("检测登录状态：");
        if (session.getAttribute("User")!=null)
        {
            System.out.println("存在登录用户");
            User user = (User) session.getAttribute("User");
            System.out.println(user.getUserAccount());
            model.addAttribute("username",user.getUserAccount());
        }else
        {
            System.out.println("未登录");
        }
        return "User";
    }

    //登录
    @RequestMapping("/Login")
    @ResponseBody
    public String Login(String username,String password,Model model, HttpSession session)
    {
        //查询是否存在账号
        if (userService.checkUserAccountRepeated(username))
        {
            //查询账号密码是否对应
            if (userService.checkUserPass(username,password))
            {
                User user = userService.selectUserByAccount(username);
                session.setAttribute("User",user);
                session.setAttribute("userId",user.getUserId());
                return "登录成功";
            }else {
                return "密码错误";
            }
        }else {
            User user = new User();
            user.setUserAccount(username);
            user.setUserPassword(password);
            user.setUserRoles(1);
            int userId = userService.register(user);
            //没有返回正确的主键导致无法记录正常的UserId
//            user.setUserId(userId);
//
//            session.setAttribute("User",user);
//            session.setAttribute("userId",userId);
//            System.out.println("注册ID"+userId+"  对象ID:"+user.getUserId());
            return "注册成功";
        }
    }

    //注销
    @RequestMapping("/Logout")
    public String Logout(Model model, HttpSession session)
    {
        session.invalidate();
        return "User";
    }
}
