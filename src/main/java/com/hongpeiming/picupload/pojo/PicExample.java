package com.hongpeiming.picupload.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PicExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PicExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPicIdIsNull() {
            addCriterion("pic_id is null");
            return (Criteria) this;
        }

        public Criteria andPicIdIsNotNull() {
            addCriterion("pic_id is not null");
            return (Criteria) this;
        }

        public Criteria andPicIdEqualTo(Integer value) {
            addCriterion("pic_id =", value, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdNotEqualTo(Integer value) {
            addCriterion("pic_id <>", value, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdGreaterThan(Integer value) {
            addCriterion("pic_id >", value, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("pic_id >=", value, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdLessThan(Integer value) {
            addCriterion("pic_id <", value, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdLessThanOrEqualTo(Integer value) {
            addCriterion("pic_id <=", value, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdIn(List<Integer> values) {
            addCriterion("pic_id in", values, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdNotIn(List<Integer> values) {
            addCriterion("pic_id not in", values, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdBetween(Integer value1, Integer value2) {
            addCriterion("pic_id between", value1, value2, "picId");
            return (Criteria) this;
        }

        public Criteria andPicIdNotBetween(Integer value1, Integer value2) {
            addCriterion("pic_id not between", value1, value2, "picId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdIsNull() {
            addCriterion("pic_uploader_id is null");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdIsNotNull() {
            addCriterion("pic_uploader_id is not null");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdEqualTo(Integer value) {
            addCriterion("pic_uploader_id =", value, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdNotEqualTo(Integer value) {
            addCriterion("pic_uploader_id <>", value, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdGreaterThan(Integer value) {
            addCriterion("pic_uploader_id >", value, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("pic_uploader_id >=", value, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdLessThan(Integer value) {
            addCriterion("pic_uploader_id <", value, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdLessThanOrEqualTo(Integer value) {
            addCriterion("pic_uploader_id <=", value, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdIn(List<Integer> values) {
            addCriterion("pic_uploader_id in", values, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdNotIn(List<Integer> values) {
            addCriterion("pic_uploader_id not in", values, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdBetween(Integer value1, Integer value2) {
            addCriterion("pic_uploader_id between", value1, value2, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicUploaderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("pic_uploader_id not between", value1, value2, "picUploaderId");
            return (Criteria) this;
        }

        public Criteria andPicNameIsNull() {
            addCriterion("pic_name is null");
            return (Criteria) this;
        }

        public Criteria andPicNameIsNotNull() {
            addCriterion("pic_name is not null");
            return (Criteria) this;
        }

        public Criteria andPicNameEqualTo(String value) {
            addCriterion("pic_name =", value, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameNotEqualTo(String value) {
            addCriterion("pic_name <>", value, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameGreaterThan(String value) {
            addCriterion("pic_name >", value, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameGreaterThanOrEqualTo(String value) {
            addCriterion("pic_name >=", value, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameLessThan(String value) {
            addCriterion("pic_name <", value, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameLessThanOrEqualTo(String value) {
            addCriterion("pic_name <=", value, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameLike(String value) {
            addCriterion("pic_name like", value, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameNotLike(String value) {
            addCriterion("pic_name not like", value, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameIn(List<String> values) {
            addCriterion("pic_name in", values, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameNotIn(List<String> values) {
            addCriterion("pic_name not in", values, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameBetween(String value1, String value2) {
            addCriterion("pic_name between", value1, value2, "picName");
            return (Criteria) this;
        }

        public Criteria andPicNameNotBetween(String value1, String value2) {
            addCriterion("pic_name not between", value1, value2, "picName");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathIsNull() {
            addCriterion("pic_url_path is null");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathIsNotNull() {
            addCriterion("pic_url_path is not null");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathEqualTo(String value) {
            addCriterion("pic_url_path =", value, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathNotEqualTo(String value) {
            addCriterion("pic_url_path <>", value, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathGreaterThan(String value) {
            addCriterion("pic_url_path >", value, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathGreaterThanOrEqualTo(String value) {
            addCriterion("pic_url_path >=", value, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathLessThan(String value) {
            addCriterion("pic_url_path <", value, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathLessThanOrEqualTo(String value) {
            addCriterion("pic_url_path <=", value, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathLike(String value) {
            addCriterion("pic_url_path like", value, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathNotLike(String value) {
            addCriterion("pic_url_path not like", value, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathIn(List<String> values) {
            addCriterion("pic_url_path in", values, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathNotIn(List<String> values) {
            addCriterion("pic_url_path not in", values, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathBetween(String value1, String value2) {
            addCriterion("pic_url_path between", value1, value2, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUrlPathNotBetween(String value1, String value2) {
            addCriterion("pic_url_path not between", value1, value2, "picUrlPath");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataIsNull() {
            addCriterion("pic_upload_data is null");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataIsNotNull() {
            addCriterion("pic_upload_data is not null");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataEqualTo(Date value) {
            addCriterion("pic_upload_data =", value, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataNotEqualTo(Date value) {
            addCriterion("pic_upload_data <>", value, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataGreaterThan(Date value) {
            addCriterion("pic_upload_data >", value, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataGreaterThanOrEqualTo(Date value) {
            addCriterion("pic_upload_data >=", value, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataLessThan(Date value) {
            addCriterion("pic_upload_data <", value, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataLessThanOrEqualTo(Date value) {
            addCriterion("pic_upload_data <=", value, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataIn(List<Date> values) {
            addCriterion("pic_upload_data in", values, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataNotIn(List<Date> values) {
            addCriterion("pic_upload_data not in", values, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataBetween(Date value1, Date value2) {
            addCriterion("pic_upload_data between", value1, value2, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicUploadDataNotBetween(Date value1, Date value2) {
            addCriterion("pic_upload_data not between", value1, value2, "picUploadData");
            return (Criteria) this;
        }

        public Criteria andPicMd5IsNull() {
            addCriterion("pic_md5 is null");
            return (Criteria) this;
        }

        public Criteria andPicMd5IsNotNull() {
            addCriterion("pic_md5 is not null");
            return (Criteria) this;
        }

        public Criteria andPicMd5EqualTo(String value) {
            addCriterion("pic_md5 =", value, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5NotEqualTo(String value) {
            addCriterion("pic_md5 <>", value, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5GreaterThan(String value) {
            addCriterion("pic_md5 >", value, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5GreaterThanOrEqualTo(String value) {
            addCriterion("pic_md5 >=", value, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5LessThan(String value) {
            addCriterion("pic_md5 <", value, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5LessThanOrEqualTo(String value) {
            addCriterion("pic_md5 <=", value, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5Like(String value) {
            addCriterion("pic_md5 like", value, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5NotLike(String value) {
            addCriterion("pic_md5 not like", value, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5In(List<String> values) {
            addCriterion("pic_md5 in", values, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5NotIn(List<String> values) {
            addCriterion("pic_md5 not in", values, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5Between(String value1, String value2) {
            addCriterion("pic_md5 between", value1, value2, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicMd5NotBetween(String value1, String value2) {
            addCriterion("pic_md5 not between", value1, value2, "picMd5");
            return (Criteria) this;
        }

        public Criteria andPicSizeIsNull() {
            addCriterion("pic_size is null");
            return (Criteria) this;
        }

        public Criteria andPicSizeIsNotNull() {
            addCriterion("pic_size is not null");
            return (Criteria) this;
        }

        public Criteria andPicSizeEqualTo(Integer value) {
            addCriterion("pic_size =", value, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeNotEqualTo(Integer value) {
            addCriterion("pic_size <>", value, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeGreaterThan(Integer value) {
            addCriterion("pic_size >", value, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeGreaterThanOrEqualTo(Integer value) {
            addCriterion("pic_size >=", value, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeLessThan(Integer value) {
            addCriterion("pic_size <", value, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeLessThanOrEqualTo(Integer value) {
            addCriterion("pic_size <=", value, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeIn(List<Integer> values) {
            addCriterion("pic_size in", values, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeNotIn(List<Integer> values) {
            addCriterion("pic_size not in", values, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeBetween(Integer value1, Integer value2) {
            addCriterion("pic_size between", value1, value2, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicSizeNotBetween(Integer value1, Integer value2) {
            addCriterion("pic_size not between", value1, value2, "picSize");
            return (Criteria) this;
        }

        public Criteria andPicTypeIsNull() {
            addCriterion("pic_type is null");
            return (Criteria) this;
        }

        public Criteria andPicTypeIsNotNull() {
            addCriterion("pic_type is not null");
            return (Criteria) this;
        }

        public Criteria andPicTypeEqualTo(String value) {
            addCriterion("pic_type =", value, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeNotEqualTo(String value) {
            addCriterion("pic_type <>", value, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeGreaterThan(String value) {
            addCriterion("pic_type >", value, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeGreaterThanOrEqualTo(String value) {
            addCriterion("pic_type >=", value, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeLessThan(String value) {
            addCriterion("pic_type <", value, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeLessThanOrEqualTo(String value) {
            addCriterion("pic_type <=", value, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeLike(String value) {
            addCriterion("pic_type like", value, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeNotLike(String value) {
            addCriterion("pic_type not like", value, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeIn(List<String> values) {
            addCriterion("pic_type in", values, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeNotIn(List<String> values) {
            addCriterion("pic_type not in", values, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeBetween(String value1, String value2) {
            addCriterion("pic_type between", value1, value2, "picType");
            return (Criteria) this;
        }

        public Criteria andPicTypeNotBetween(String value1, String value2) {
            addCriterion("pic_type not between", value1, value2, "picType");
            return (Criteria) this;
        }

        public Criteria andPicRemarkIsNull() {
            addCriterion("pic_remark is null");
            return (Criteria) this;
        }

        public Criteria andPicRemarkIsNotNull() {
            addCriterion("pic_remark is not null");
            return (Criteria) this;
        }

        public Criteria andPicRemarkEqualTo(String value) {
            addCriterion("pic_remark =", value, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkNotEqualTo(String value) {
            addCriterion("pic_remark <>", value, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkGreaterThan(String value) {
            addCriterion("pic_remark >", value, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("pic_remark >=", value, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkLessThan(String value) {
            addCriterion("pic_remark <", value, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkLessThanOrEqualTo(String value) {
            addCriterion("pic_remark <=", value, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkLike(String value) {
            addCriterion("pic_remark like", value, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkNotLike(String value) {
            addCriterion("pic_remark not like", value, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkIn(List<String> values) {
            addCriterion("pic_remark in", values, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkNotIn(List<String> values) {
            addCriterion("pic_remark not in", values, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkBetween(String value1, String value2) {
            addCriterion("pic_remark between", value1, value2, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicRemarkNotBetween(String value1, String value2) {
            addCriterion("pic_remark not between", value1, value2, "picRemark");
            return (Criteria) this;
        }

        public Criteria andPicStatusIsNull() {
            addCriterion("pic_status is null");
            return (Criteria) this;
        }

        public Criteria andPicStatusIsNotNull() {
            addCriterion("pic_status is not null");
            return (Criteria) this;
        }

        public Criteria andPicStatusEqualTo(Integer value) {
            addCriterion("pic_status =", value, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusNotEqualTo(Integer value) {
            addCriterion("pic_status <>", value, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusGreaterThan(Integer value) {
            addCriterion("pic_status >", value, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("pic_status >=", value, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusLessThan(Integer value) {
            addCriterion("pic_status <", value, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusLessThanOrEqualTo(Integer value) {
            addCriterion("pic_status <=", value, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusIn(List<Integer> values) {
            addCriterion("pic_status in", values, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusNotIn(List<Integer> values) {
            addCriterion("pic_status not in", values, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusBetween(Integer value1, Integer value2) {
            addCriterion("pic_status between", value1, value2, "picStatus");
            return (Criteria) this;
        }

        public Criteria andPicStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("pic_status not between", value1, value2, "picStatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}