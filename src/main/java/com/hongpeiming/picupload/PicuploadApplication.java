package com.hongpeiming.picupload;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@MapperScan("com.hongpeiming.picupload.mapper")
@SpringBootApplication
public class PicuploadApplication {



    public static void main(String[] args) {
        SpringApplication.run(PicuploadApplication.class, args);
    }

}
